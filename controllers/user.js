'use strict'

const bcrypt = require('bcrypt-nodejs');
const User = require('../models/user');

const pruebas = (req, res) => {
    res.status(200).send({
        message: 'Probando una accion del controlador de usuarios del api rest con node y mongo'
    })
}

const saveUser = (req, res) => {
    const user = new User();
    const params = req.body;

    console.log(); 
    
    user.name = params.name;
    user.lastname = params.lastname;
    user.email = params.email;
    user.role = 'ROLE_USER';
    user.image = 'null';

    if(params.password){
        //Encriptar contraseña y guardar datos
    } else {
        res.status(500).send({message: 'Introduce la contraseña'});
    }

}

module.exports = {
    pruebas
};