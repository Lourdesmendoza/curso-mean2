'use strict'

const express = require('express');
const bodyParser = require('body-parser');

const app = express();

// Load path
const user_routes = require('./routes/user');

//bodyParser Setting
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

// configure http headers

// Base path
app.use('/api', user_routes);

module.exports = app;
